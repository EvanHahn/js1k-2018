const express = require('express')
const path = require('path')
const readSource = require('./read-source')
const minify = require('./minify')

const app = express()

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.get('/', async (req, res) => {
  const output = await minify()
  const byteCount = Buffer.from(output, 'utf8').length
  res.render('shim', { output, byteCount })
})

app.get('/dev', async (req, res) => {
  const outputBuffer = await readSource()
  const byteCount = outputBuffer.length
  const output = outputBuffer.toString('utf8')
  res.render('shim', { output, byteCount })
})

const port = process.env.PORT || 3000
app.listen(port, () => {
  console.log('App started on :' + port)
})
