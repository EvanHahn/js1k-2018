const path = require('path')
const util = require('util')
const childProcess = require('child_process')
const RegPack = require('regpack').RegPack
const readSource = require('./read-source')

const execFile = util.promisify(childProcess.execFile)

const SOURCE_PATH = path.join(__dirname, 'js1k.js')
const BIN_PATH = path.join(__dirname, '..', 'node_modules', '.bin')
const BABEL_MINIFY_PATH = path.join(BIN_PATH, 'minify')
const BABEL_MINIFY_OPTIONS = ['--builtIns', '--booleans', '--mangle', '--infinity', '--flipComparisons', '--mangle.topLevel']

module.exports = async function minify () {
  const rawSource = await readSource()
  const babelResult = await babelMinify()
  const regPackResult = regpackMinify(babelResult)

  console.log()
  console.log('Raw source size: ' + rawSource.length)
  console.log('Babel minify size: ' + babelResult.length)
  console.log('Regpack size: ' + regPackResult.length)

  const results = [rawSource, babelResult, regPackResult]

  return shortest(results).toString('utf8').trim()
}

function shortest (arr) {
  return arr.reduce((result, value) => {
    return result.length < value.length ? result : value
  }, arr[0]).toString('utf8').trim()
}

async function babelMinify () {
  const result = await execFile(BABEL_MINIFY_PATH, [SOURCE_PATH].concat(BABEL_MINIFY_OPTIONS))
  const withoutTrailingSemicolon = result.stdout.replace(/;$/, '')
  return Buffer.from(withoutTrailingSemicolon, 'utf8')
}

function regpackMinify (sourceBuffer) {
  const source = sourceBuffer.toString('utf8')
  const regpack = new RegPack()
  /*
  You can try this to try all possible options:

  const packerResults = allPossibleObjects({
    withMath: [true, false],
    crushGainFactor: [0, 1, 2],
    crushLengthFactor: [0, 1],
    crushCopiesFactor: [0, 1],
    crushTiebreakerFactor: [0, 1]
  })
    .map((options) => Object.assign({
      hash2DContext: true,
      contextVariableName: 'c',
      reassignVars: true,
      varsNotReassigned: ['a', 'b', 'c', 'd']
    }, options))
    .map((options) => regpack.runPacker(source, options))
    .reduce((flattened, arr) => flattened.concat(arr), [])
  */
  const packerResults = regpack.runPacker(source, {
    withMath: true,
    hash2DContext: false,
    reassignVars: true,
    varsNotReassigned: ['a', 'b', 'c']
  })
  const resultStrings = packerResults.reduce((resultStrings, packerResult) => {
    const packerResultStrings = packerResult.result.map((r) => r[1])
    return resultStrings.concat(packerResultStrings)
  }, [])
  return Buffer.from(shortest(resultStrings))
}
