/*
 * a = <canvas>
 * b = <body>
 * B = Bolt "class"
 * c = context
 * e = array of entities
 * k = tick function
 * m = mouse object
 * r = scalar
 * s = delta time
 * S = Spark "class"
 * t = current time
 * v = isVisible?
 * x = speed of time (starts at 1)
 */

/* eslint-disable no-undef */

/* Initial setup */

a.style.position = `absolute`
b.style.overflow = `hidden`
b.style.background = `#000`

e = []

r = Math.min(a.width, a.height)

m = {
  x: a.width / 2,
  y: a.height / 2
}

x = 1

// Is a point visible?

v = (point) => {
  return (
    (point.x > 0) &&
    (point.x < a.width) &&
    (point.y > 0) &&
    (point.y < a.height)
  )
}

// Bolt

B = () => {
  // Calculate the list of points, starting at the mouse

  let lastPoint = { x: m.x, y: m.y }
  let points = [lastPoint]

  let direction = Math.random() * Math.PI * 2
  while (v(lastPoint)) {
    let newLineLength = Math.random() * r / 6
    direction += (Math.random() * Math.PI / 1.5) - (Math.PI / 3)

    lastPoint = {
      x: lastPoint.x + (Math.sin(direction) * newLineLength),
      y: lastPoint.y + (Math.cos(direction) * newLineLength)
    }
    points.push(lastPoint)
  }

  points.reverse()

  // Other startin values

  let width = (Math.random() * r / 10) + (r / 20)

  let red = Math.random() * 255
  let green = Math.random() * 255
  let blue = Math.random() * 255
  let alpha = 1
  let noSparksYet = 1 // used as true/false

  let timeAlive = 0
  let timeToFinish = (Math.random() * 200) + 100

  // Jolt the screen

  a.style.top = Math.round((Math.random() * 6) - 3) + `px`
  a.style.left = Math.round((Math.random() * 6) - 3) + `px`

  // Return value

  let me = () => {
    timeAlive += s

    alpha -= s / 1000

    if (alpha < 0) {
      e.splice(e.indexOf(me), 1)
    }

    if (timeAlive > timeToFinish) {
      c.lineWidth = width
      width = Math.max(2, width - s)

      if (noSparksYet) {
        points.map((point) => e.push(S(point)))
        noSparksYet = 0
      }
    } else {
      c.lineWidth = 2
    }

    c.strokeStyle = `rgba(${[
      red | 0,
      green | 0,
      blue | 0,
      alpha
    ]})`
    c.lineCap = `round`

    c.beginPath()
    c.moveTo(points[0].x, points[0].y)
    points
      .slice(0, timeAlive * points.length / timeToFinish)
      .map((point) => c.lineTo(point.x, point.y))
    c.stroke()
  }

  return me
}

// Spark

S = ({ x, y }) => {
  let size = 10
  let speed = Math.random() / 2

  let direction = Math.random() * Math.PI * 2

  let alpha = 1

  let me = () => {
    x += Math.sin(direction) * speed * s
    y += Math.cos(direction) * speed * s

    alpha -= s / 200
    if (alpha < 0) {
      e.splice(e.indexOf(me), 1)
    }

    c.fillStyle = `rgba(255,255,255,${alpha})`
    c.fillRect(x - (size / 2), y - (size / 2), size, size)
  }

  return me
}

// Tick

let t = 0
requestAnimationFrame(k = (currentTime) => {
  s = (currentTime - t) * x
  t = currentTime

  c.clearRect(0, 0, a.width, a.height)

  if (Math.random() < (0.2 * x)) {
    e.push(B())
  }

  e.map((entity) => entity())

  requestAnimationFrame(k)
})

onmousemove = (event) => {
  m.x = event.clientX
  m.y = event.clientY
}

onkeydown = (event) => {
  if (event.keyCode === 40) {
    x = Math.max(0.1, x - 0.1)
  }
  if (event.keyCode === 38) {
    x = Math.min(2, x + 0.1)
  }
}
