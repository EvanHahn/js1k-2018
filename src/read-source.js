const path = require('path')
const fs = require('fs')
const util = require('util')

const readFile = util.promisify(fs.readFile)

const SOURCE_PATH = path.join(__dirname, 'js1k.js')

module.exports = async function readSource () {
  return readFile(SOURCE_PATH)
}
