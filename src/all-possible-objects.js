module.exports = function allPossibleObjects (options) {
  const keys = Object.keys(options)
  if (keys.length === 0) { return {} }

  const numberOfResults = Object.values(options).reduce((count, choices) => count * choices.length, 1)

  const results = []
  for (let i = 0; i < numberOfResults; i++) {
    results.push(keys.reduce((result, key) => {
      const choices = options[key]
      return {
        ...result,
        [key]: choices[i % choices.length]
      }
    }, {}))
  }

  return results
}
