const allPossibleObjects = require('../src/all-possible-objects')

describe('allPossibleObjects', () => {
  it('returns an array of objects with all possible options', () => {
    const result = allPossibleObjects({
      one: ['foo'],
      bool: [true, false],
      nums: [1, 2, 3]
    })

    expect(result).toHaveLength(6)

    ;[true, false].forEach((bool) => {
      expect(result).toContainEqual({
        one: 'foo',
        bool,
        nums: 1
      })
      expect(result).toContainEqual({
        one: 'foo',
        bool,
        nums: 2
      })
      expect(result).toContainEqual({
        one: 'foo',
        bool,
        nums: 3
      })
    })
  })
})
