JS1k 2018 entry: "Zap"
======================

See it [here](https://js1k.com/2018-coins/details/3159).

Run `npm start` to run the server. Visiting `/` will give you the compressed version and visiting `/dev` will give you an uncompressed version.